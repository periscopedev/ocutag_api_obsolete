<?php
/**
 * Definition of the configuration object for the Ocutag API
 *
 * @author Torsten Muller <tmuller@periscope.com>
 *
 * @package Ocutag
 * @category
 * @subcategory
 * Date: 11/14/13 9:59 AM
 */

namespace Ocutag;

/**
 * Class Config
 *
 * @package Ocutag
 */
class Config
{

    /**
     * Options required for interfacing with the Ocutag API
     *
     * @var array
     */
    protected $config_opts = array(
        'DEV_IDENTIFIER'    => null,
        'DEV_SHARED_SECRET' => null,
        'APP_IDENTIFIER'    => null,
        'APP_SHARED_SECRET' => null,
        'APP_NAME'          => null,
        'COLLECTION_NAME'   => null,
    );


    /**
     * Constructor which will parse and manage the API validation credentials
     *
     * @param null $config Configuration entity. Parses arrays, reads ini files
     *                     or checks the environment
     *
     * @return void
     */
    public function __construct($config = null)
    {
        if (is_null($config)) {
            // TODO: Read the configuration from the environment
        } elseif (is_string($config) && strpos('/', $config) !== false) {
            // TODO: It's a file path. Assume it's .ini and try to parse
        } elseif (is_array($config)) {
            // TODO: Parse the passed-in array into config options
        }
    }


    /**
     * Method to retrieve the API credentials from the Apache configuration. It is case-insensitive
     * and expects all parameters to be set in Apache using SetEnv
     *
     * @return void
     */
    public function parseFromEnvironment()
    {
        foreach ($this->config_opts as $key => $value) {

        }

    }


}
