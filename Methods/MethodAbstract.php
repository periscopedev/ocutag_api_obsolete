<?php
/**
 * Description
 *
 * @author Torsten Muller <tmuller@periscope.com>
 *
 * @package
 * @category
 * @subcategory
 * Date: 11/14/13 10:14 AM
 */

namespace Ocutag\Methods;

/**
 * Class MethodAbstract
 *
 * @package Ocutag\Methods
 * @author  Torsten Muller <tmuller@periscope.com>
 */
abstract class MethodAbstract
{

    /**
     * Stores the request type to send to Ocutag. This value should be overwritten by an
     * child classes, according to the method to be called
     *
     * @var string
     */
    protected $request_type = 'POST';


    /**
     * Returns the type of HTTP request to make
     *
     * @return string
     */
    public function getReqestType()
    {
        return $this->request_type;
    }


    /**
     * Provider method to return all the data required to push to the Ocutag API
     *
     * @return mixed
     */
    abstract public function getRequestData();


}
