# Ocutag API #

This repo contains PHP code to interface with the [Ocutag](http://ocutag.com) image API from a server.
It is a work in progress and is currently built on sample code developed for demo purposes.


## Usage ##

Documentation yet to come.

## License ##

This code is licenses under the MIT license.


## Acknowledgments ##

Thanks for [Periscope](http://periscope.com) for allowing me time to explore new directions and
play with new technologies. Thanks also go to Mike Griffin, Bharathi Shekar and Nagesh Shenoy at
Ocutag for helping me getting the authenication figured out.
