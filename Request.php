<?php
/**
 * Simple file to process the uploaded image data and save it into a file
 *
 * @author Torsten Muller <tmuller@periscope.com>
 *
 * @package Ocutag
 * @category
 * @subcategory
 */

namespace Ocutag;


/**
 * Class Request
 *
 * @category Ocutag
 * @package  Ocutag
 * @author   Torsten Muller <tmuller@periscope.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/periscopedev/ocutag_api
 */
class Request
{

    /**
     * A bunch of constants which contain the API creds and other information for
     * making the API call.
     */
    const DEV_IDENTIFIER = '4a30d64f1cd30d375db203c899135f3f3d921980';
    const DEV_SHARED_SECRET = '684f625145786e674d734b61426e304470505648764466594e354c565a554f';

    const APP_IDENTIFIER = '05ee909842402cb3b14426b9eaa6dc0d9eddc636';
    const APP_SHARED_SECRET = '74647445744a5155316f486b37585834543245496d614976714435554d7541';

    const APP_NAME = 'PhotoTest';
    const COLLECTION_NAME = 'TravelPhotos';

    const MATCH_URL = 'http://api.ocutag.com/v1/collections/%%COLLECTION_NAME%%';


    /**
     * Stored data of the submitted image.
     *
     * @var null
     */
    protected $binaryImageData = null;


    /**
     * Raw image data, as submitted by front-end
     *
     * @var null
     */
    protected $base64Image = null;


    /**
     * Cache for the generated random string for the authentication
     *
     * @var string
     */
    protected $randomString = '';



    protected $_statusObj = null;


    /**
     * Constructor, which saves the configuration object into a usable property
     *
     * @param $config \Ocutag\Config object for setup of the API credentials
     *
     * @return void
     */
    public function __construct($config)
    {
        $this->_statusObj = $config;
    }


    /**
     * Public method which takes the sent image data, stores it on the file system and
     * initiates the communication with the Ocutag API
     *
     * @return void
     */
    public function processImageSubmission()
    {
        $raw_data = file_get_contents('php://input');
        $this->binaryImageData = base64_decode(substr($raw_data, 23));
        $filename = md5($this->binaryImageData) . '.jpg';
        file_put_contents(dirname(__FILE__) . '/img/' . $filename, $this->binaryImageData);
    }


    /**
     * Method that preps the data for the actual request to the API. It sets all the correct
     * headers, generates the Auth header and calls the method to actually perform the API request.
     *
     * @return void
     */
    public function sendMatchRequest()
    {
        $auth_headers = $this->generateAuthHeaderArray();
        $auth_headers['oas_signature'] = $this->generateRequestSignature();

        $auth_comps = array();
        foreach ($auth_headers as $key => $value) {
            $auth_comps[] = $key . '=' . $value;
        }

        $request_headers = $this->generateHeaders();
        $request_headers['Authorization'] = 'OAS ' . implode(', ', $auth_comps);
        $request_headers['Content-Length'] = strlen($this->binaryImageData);
        $request_headers['Accept'] = '*/*';
        unset($request_headers['match']);

        $reply = $this->sendHttpRequest($request_headers, $this->binaryImageData);
        $this->processOcutagReply($reply);

        return $this->_statusObj;
    }


    /**
     * Method to send a request to the Ocutag API
     *
     * @param array  $headers Associative array of the headers to set.
     * @param string $body    String of the request body to send
     *
     * @return string         The reply from the Ocutag API
     */
    protected function sendHttpRequest($headers, $body)
    {
        $fp = fsockopen($this->buildRequestUrl('host'), 80, $errno, $errstr, 25);
        $server_reply = '';
        if (!$fp) {
            echo "$errstr ($errno)\n";
        } else {
            $out = 'POST ' . $this->buildRequestUrl('path') . "?match HTTP/1.1\r\n";
            $out .= "Host: " . $this->buildRequestUrl('host') . "\r\n";
            foreach ($headers as $key => $value) {
                $out .= $key . ': ' . $value . "\r\n";
            }
            $out .= "Connection: Close\r\n\r\n";
            $out .= $body;

            fwrite($fp, $out);
            while (!feof($fp)) {
                $server_reply .= fgets($fp, 128);
            }
            fclose($fp);
        }

        return $server_reply;
    }


    /**
     * Method processes the raw reply from the Ocutag API and processes the content
     * according to the reply type
     *
     * @param string $raw_reply HTTP header and body returned by the Ocutag API
     *
     * @return void
     */
    protected function processOcutagReply($raw_reply)
    {
        if (empty($raw_reply)) {
            $this->processServerError('05', array(), array());
        }

        $rows = explode("\r\n", $raw_reply . "\r\n");
        preg_match('/HTTP\/1\.. (\d)(\d{2})/', $rows[0], $match);

        $body = $this->getBody($rows);
        $headers = $this->parseHeaders($rows);

        switch ($match[1]) {

            case '2':
                $this->processMatchSuccess($match[2], $headers, $body);
                break;

            case '4':
                $this->processRequestError($match[2], $headers, $body);
                break;

            case '5':
                $this->processServerError($match[2], $headers, $body);
                break;

        }
    }


    /**
     * Parses the headers from an indexed array into the proper associated array with
     * header keys as array keys
     *
     * @param array $rows An array of strings, with each array entry being the raw string
     *                    from the Ocutag HTTP response.
     *
     * @return array      An associative array of the headers parsed into key/value pairs
     */
    protected function parseHeaders($rows)
    {
        $headers = array();
        foreach ($rows as $h) {
            if (empty($h)) {
                // If we reach the end of the headers and get into the body, we're done
                // and break out of the parsing loop.
                break;
            }

            $colon_id = strpos($h, ':');
            if ($colon_id === false) {
                continue;
            }
            $key = substr($h, 0, $colon_id);
            $value = substr($h, $colon_id + 2);
            $headers[$key] = $value;
        }

        return $headers;
    }


    /**
     * Extract the body of the message from the reply, already split into an array
     *
     * @param array $headers An indexed array of the raw reply, with each entry representing
     *                       a row of the HTTP response
     *
     * @return array
     */
    protected function getBody($headers)
    {
        $is_body = false;
        $body_rows = array();
        foreach ($headers as $row) {
            if (empty($row)) {
                $is_body = true;
                continue;
            }

            if ($is_body) {
                $body_rows[] = $row;
            }
        }

        return $body_rows;
    }


    /**
     * Processes a successful match request to the Ocutag API and populates the statusObj
     * with the matches found
     *
     * @param string $code       Last two digits of the HTTP status code, e.g. "00" for a 200 response
     * @param array  $full_reply Associative array of headers from the response
     * @param array  $body       List of the matches returned by the API
     *
     * @return void
     */
    protected function processMatchSuccess($code, $full_reply, $body)
    {
        $match_found = (bool)$full_reply['X-Otag-Match'];

        $this->_statusObj->apiSuccess = true;
        $this->_statusObj->matches = array();

        if ($match_found) {
            $num_matches = count($body);
            for ($i = 0; $i < $num_matches; $i++) {
                $this->_statusObj->matches[] = $body[$i];
            }
        }
    }


    /**
     * Processes error replies with status codes of 4xx, e.g. not found. Currently only sets the
     * status to fail
     *
     * @param string $code       Last two digits of the HTTP status code, e.g. "00" for a 200 response
     * @param array  $full_reply Associative array of headers from the response
     * @param array  $body       List of the matches returned by the API
     *
     * @return void
     */
    protected function processRequestError($code, $full_reply, $body)
    {
        $this->_statusObj->apiSuccess = false;
    }


    /**
     * Processes error replies with status codes of 4xx, e.g. not found. Currently only sets the
     * status to fail
     *
     * @param string $code       Last two digits of the HTTP status code, e.g. "00" for a 200 response
     * @param array  $full_reply Associative array of headers from the response
     * @param array  $body       List of the matches returned by the API
     *
     * @return void
     */
    protected function processServerError($code, $full_reply, $body)
    {
        $this->_statusObj->apiSuccess = false;
    }


    /**
     * Fires off the request to the Ocutag API, being passed the headers and body generated
     * elsewhere in this object.
     *
     * @param array  $headers Indexed array of full headers: array('Content-type: text/html');
     * @param string $body    String of the body to send
     *
     * @return void
     */
    protected function doCurlRequest($headers, $body)
    {
        $curl = curl_init();

        $curl_opts = array(
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_URL => $this->buildRequestUrl(),
            CURLOPT_HTTPHEADER => $this->formatHeadersForCurl($headers),
        );

        curl_setopt_array($curl, $curl_opts);
        $reply = curl_exec($curl);

        $fp = fopen('api.log', 'w');
        fwrite($fp, $reply . "\n\n");
        fclose($fp);
    }


    /**
     * Convenience method that synthesizes the actual request URL from the URL pattern and
     * the collection name.
     *
     * @param string $part String indicating which part of the URL to return. This
     *                     should be the same strings/constants as in parse_url method
     *
     * @return mixed
     */
    protected function buildRequestUrl($part = null)
    {
        $full_url = str_replace('%%COLLECTION_NAME%%', self::COLLECTION_NAME, self::MATCH_URL);
        if (empty($part)) {
            return $full_url;
        }
        $comps = parse_url($full_url);
        return $comps[strtolower($part)];
    }


    /**
     * Converts an associative array with key/value pairs into an indexed array
     * where keys and values are separated by a colon ":", as this is what the
     * CURL library expects.
     *
     * @param array $headers_assoc Associative array of key/value pairs which will
     *                            be turned into an indexed array of header strings for cURL
     *
     * @return array
     */
    protected function formatHeadersForCurl($headers_assoc)
    {
        $headers_indexed = array();
        foreach ($headers_assoc as $key => $value) {
            $headers_indexed[] = $key . ': ' . $value;
        }

        return $headers_indexed;
    }


    /**
     * Method that generates the individual segments of the Auth header, except for the
     * signature. The value generation follows the instructions on the web site:
     *
     * https://ocutag.com/documents/authentication
     *
     * @return array
     */
    protected function generateAuthHeaderArray()
    {
        $auth_components = array(
            'realm' => 'ocutag',
            'oas_version' => '1.0',
            'oas_client_id' => base64_encode($this->stringToByteSequence(self::APP_IDENTIFIER)),
            'oas_access_id' => base64_encode($this->stringToByteSequence(self::DEV_IDENTIFIER)),
            'oas_nonce' => $this->generateRandomString(),
        );

        return $auth_components;
    }


    /**
     * Proxy method that generates the key and value, then hashes and base64 encodes the result
     * according to the instructions on the Ocutag web site:
     *
     * https://ocutag.com/documents/authentication
     *
     * @return string
     */
    protected function generateRequestSignature()
    {
        $key_string = $this->stringToByteSequence(self::DEV_SHARED_SECRET) . '&&'
                    . $this->stringToByteSequence(self::APP_SHARED_SECRET);

        $sig_base_string = $this->generateSignatureBaseString();
        $hash = hash_hmac('sha1', $sig_base_string, $key_string);

        return base64_encode($this->stringToByteSequence($hash));
    }


    /**
     * A caching random string method. A random string will be generated the first time and
     * stored so it can be returned the next time this method is invoked. The caching is necessary
     * as the random string method gets called at least twice during the auth token generation and
     *
     * @return string
     */
    protected function generateRandomString()
    {
        if (!empty($this->randomString)) {
            return $this->randomString;
        }

        $char_list = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM';
        $num_options = strlen($char_list);

        $length = rand(12, 40);
        $rnd_string = '';
        for ($i=0; $i < $length; $i++) {
            $rnd_string .= substr($char_list, rand(0, $num_options - 1), 1);
        }

        $this->randomString = str_shuffle($rnd_string);
        return $this->randomString;
    }


    /**
     * Method that turns a string of hex numbers into the corresponding byte sequence. Two
     * chars each are taken and converted into the corresponding ASCII character
     *
     * @param string $hex_string A string of hex-numbers
     *
     * @return string
     */
    protected function stringToByteSequence($hex_string)
    {
        $bytes = strlen($hex_string);

        $output = '';
        for ($i=0; $i < $bytes; $i += 2) {
            $output .= chr(hexdec(substr($hex_string, $i, 2)));
        }

        return $output;
    }


    /**
     * Method to generate an array of the headers that will be used in the authorization routine
     * and then later also sent to API
     *
     * @return array
     */
    protected function generateHeaders()
    {
        $headers = array(
            'match' => '',
            'Date' =>  date('D, d M Y H:i:s \G\M\T', time() - date('Z')),
            'Content-Type' => 'image/jpeg',
            'Content-MD5' => base64_encode($this->stringToByteSequence(md5($this->binaryImageData))),
        );

        return $headers;
    }


    /**
     * Method to generate the signature base string as described here:
     * https://ocutag.com/documents/signaturebasestring
     *
     * @return string
     */
    protected function generateSignatureBaseString()
    {
        // Step 1: Collect Values
        $comps = array_merge($this->generateHeaders(), $this->generateAuthHeaderArray());

        // Steps 2+3: Normalize and URL encode all values
        $sig_array = array();
        foreach ($comps as $key => $value) {
            $sig_array[strtolower($key)] = urlencode($value);
        }
        $sig_array['date'] = str_replace('+', '%20', $sig_array['date']);

        // Step 4: Order headers alphabetically
        ksort($sig_array);

        // Steps 5+6: Form a request string, url encoded
        $query_str = http_build_query($sig_array);
        $query_str = str_replace('match=', 'match', $query_str);
        $query_str = str_replace('=', '%3D', $query_str);
        $query_str = str_replace('&', '%26', $query_str);

        // Step 8: Create base URL and Request type
        $host = urlencode($this->buildRequestUrl());

        // Steps 7, 9 and 10: Combining the URL, request type and parameters
        $sig_string = 'POST&' . $host . '&' . $query_str;

        return $sig_string;
    }


}





